package com.example.models.dto.student;

import com.example.models.dto.UserDto;
import jakarta.persistence.Column;
import lombok.*;
import lombok.experimental.FieldDefaults;

 @Data
 @AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StudentDto extends UserDto{
    String  address;
    String  phoneNumber;
}
