package com.example.models.dto.teacher;

import com.example.models.dto.UserDto;
import jakarta.persistence.Column;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)

public class TeacherDto extends UserDto {

     String email;
      String password;
     Integer lessonCount;

}
