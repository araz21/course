package com.example.repository;

import com.example.models.entity.StudentEntity;
import com.example.models.entity.StudentTeacherEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StudentTeacherRepository extends JpaRepository<StudentTeacherEntity, Long> {



}
