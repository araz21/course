package com.example.service;

import com.example.models.dto.LoginRequest;
import com.example.models.dto.LoginResponse;
import com.example.models.dto.RefreshTokenRequestDto;
import com.example.models.dto.student.CreateStudentDto;
import com.example.models.dto.student.StudentDto;
import com.example.models.dto.student.UpdateStudentDto;

import java.util.List;

public interface StudentService {

    StudentDto createStudent(CreateStudentDto studentDto);

    StudentDto getById(Long id);

    List<StudentDto> getAll();

    StudentDto update(Long id, UpdateStudentDto studentDto);

    StudentDto delete(Long id);

    LoginResponse login(LoginRequest loginRequest);

    LoginResponse refresh(RefreshTokenRequestDto request);
}
