package com.example.service;

import com.example.models.dto.studentTeacher.StudentTeacherDto;
import com.example.models.dto.studentTeacher.CreateStudentTeacherDto;

public interface StudentTeacherService {
    StudentTeacherDto create(CreateStudentTeacherDto createStudentTeacherDto);
}
