package com.example.service;


import com.example.models.dto.LoginRequest;
import com.example.models.dto.LoginResponse;
import com.example.models.dto.RefreshTokenRequestDto;
import com.example.models.dto.teacher.CreateTeacherDto;
import com.example.models.dto.teacher.TeacherDto;
import com.example.models.dto.teacher.UpdateTeacherDto;

import java.util.List;

public interface TeacherService {

       LoginResponse refresh(RefreshTokenRequestDto request) ;
      TeacherDto createTeacher(CreateTeacherDto teacherDto);

      TeacherDto getById(Long id);

      List<TeacherDto> getAll();

      TeacherDto update(Long id, UpdateTeacherDto teacherDto);

      TeacherDto delete(Long id);

      LoginResponse login(LoginRequest loginRequest);
}
