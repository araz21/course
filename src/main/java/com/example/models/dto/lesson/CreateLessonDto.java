package com.example.models.dto.lesson;

import lombok.Data;

@Data
public class CreateLessonDto {

    private String title;
    private Boolean isActive;
}
